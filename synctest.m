%% Sakanashi et al test script
clear all;
% prepare inputs
[x,fs] = audioread("C:/Users/Owner/Documents/REAPER Media/HSL1to6_48000.wav");
[x2,fs2] = audioread("C:/Users/Owner/Documents/REAPER Media/HSL1to6_47998.wav");
x = x(1:48e4);
x2 = x2(1:48e4);
%x2up = resample(x2,4,1);
%x2 = [zeros(30,1); x2]; % add some delay to simulate TDOA
N = min(length(x)+fs, length(x2)+fs);
x = x(1:N-fs);
x2 = x2(1:N-fs);
eps = fs2/fs - 1;

% set up some fft stuff
fftSize = 512;
hopSize = fftSize/2;
w = hamming(fftSize+1);
w = w(1:fftSize);
scale = 1/1.08;
x2sync = zeros(N+fftSize,1);

% noise estimator
noise = zeros(round(N/hopSize),2);
noiseGuess = 10^(-50/10);
noiseavg = 0.9999999;

% init some containers and misc
epsEst = 0;
epsvec = [];
buffer = zeros(2*fftSize,2);
energy = zeros(round(N/hopSize),2);
energyIdx = 1;

xbuf1 = [];
xbuf2 = [];
vadCount = [0 0];
vad = zeros(round(N/hopSize),2);
epsg = zeros(length(vad),1);
nA = [0 0];
nB = [0 0];
D = 0;
i = [0 0];
timer = 0;
timeout = 12*fs/hopSize;
timeoutFlag = false;
%phishift = 0;
%ncentral = 0;
x1out = zeros(N+fftSize,1);
%delay1 = zeros(fftSize,1);
%ndelay = 0;
%delayout = zeros(2*fftSize,1);
%buffer1out = zeros(fftSize,1);
d = 0;
mrx = 0;
lagmax = zeros(length(vad),1);
maxrx = zeros(length(vad),1);
globalMaxRx = 0;
gmrg = zeros(length(vad),1);
hup = getInterpFiltSync();
L = round(length(hup)/2);
upsbuffer = zeros(length(hup),1);
upsframe = zeros(4*hopSize,1);
x2uphistory = zeros(90*4*fftSize,1);
% process loop
for n = 1:hopSize:N-4*hopSize
  % buffer inputs
  if n < N-fs-hopSize
    buffer(1:3*hopSize,:) = buffer(hopSize+1:end,:);
    buffer(3*hopSize+1:end,1) = x(n:n+hopSize-1);
    buffer(3*hopSize+1:end,2) = x2(n:n+hopSize-1);
  else
    buffer(1:3*hopSize,:) = buffer(hopSize+1:end,:);
    buffer(3*hopSize+1:end,:) = zeros(hopSize,2);
  end
  
  % measure energy
  energy(energyIdx,1) = buffer(1:fftSize,1)'*buffer(1:fftSize,1);
  energy(energyIdx,2) = buffer(1:fftSize,2)'*buffer(1:fftSize,2);
  % estimate noise floor
  if n > 1
    noise(energyIdx,:) = noiseavg*noise(energyIdx-1,:) + (1 - noiseavg)*energy(energyIdx,:);
  else
    noise(energyIdx,:) = noiseavg*noiseGuess + (1 - noiseavg)*energy(energyIdx,:);
  end
  % if SNR is > 10dB for 3 consecutive frames decide there is voice present
  if energy(energyIdx,1) > 10*noise(energyIdx,1)
    vadCount(1) = vadCount(1) + 1;
  else
    vadCount(1) = 0;
  end
  if energy(energyIdx,2) > 10*noise(energyIdx,2)
    vadCount(2) = vadCount(2) + 1;
  else
    vadCount(2) = 0;
  end
  
  if vadCount(1) > 3
    vad(energyIdx,1) = 1;
    % choose a "rough cut" for device 1 based on the VAD onset and timer
    if i(1) == 0 && ~timeoutFlag
      i(1) = n;
    elseif i(1) == 0 && (timer > timeout)
      i(1) = n;
    end
    if i(1) ~= 0
      xbuf1 = [xbuf1; buffer(1:hopSize,1)];
    end
  end
  if vadCount(2) > 3
    vad(energyIdx,2) = 1;
    % choose a "rough cut" for device 2 based on the VAD onset and timer
    if i(2) == 0 && ~timeoutFlag
      i(2) = n;
    elseif i(2) == 0 && (timer > timeout)
      i(2) = n;
    end
    if i(2) ~= 0
      xbuf2 = [xbuf2; buffer(1:hopSize,2)];
    end
  end
  % find the sample indices for calculating epsilon
  if i(1) ~= 0 && i(2) ~= 0
    I = abs(i(1) - i(2));
%    xbuf = zeros(fftSize + I,2);
%    xbuf(:,1) = buffer(1:fftSize+I,1);%x(i(1):i(1)+fftSize+I-1);
%    xbuf(:,2) = buffer(i(1)-i(2)+1:i(1)-i(2)+fftSize+I,2);
    len1 = length(xbuf1);
    len2 = length(xbuf2);
    if I > 0
      minlen = min(len1,len2);
      if minlen == len1
        idx  = 1;
        while len1 < len2
          xbuf1 = [xbuf1; buffer(idx*hopSize+1:idx*hopSize + hopSize,1)];
          len1 = len1 + hopSize;
          idx = idx + 1;
        end
      elseif minlen == len2
        idx  = 1;
        while len2 < len1
          xbuf2 = [xbuf2; buffer(idx*hopSize+1:idx*hopSize + hopSize,2)];
          len2 = len2 + hopSize;
          idx = idx + 1;
        end
      end
    end
    [rx,lag] = xcorr(xbuf1, xbuf2);
    [mrx,inx] = max(rx);
    d = lag(inx);
    if nA(1) == 0 && nB(1) == 0
      nA(1) = i(1) + len1/2 + d/2;
      nA(2) = i(2) + len2/2 - d/2;
      i = [0 0];
      timeoutFlag = true;
    elseif (nA(1) ~= 0 && nB(1) == 0) && (nA(2) ~= 0 && nB(2) == 0)
      nB(1) = i(1) + len1/2 + d/2;
      nB(2) = i(2) + len2/2 - d/2;
      i = [0 0];
    end
    xbuf1 = [];
    xbuf2 = [];
  end
  lagmax(energyIdx) = d;
  maxrx(energyIdx) = mrx;
  if mrx > globalMaxRx
    globalMaxRx = mrx;
  end
  gmrx(energyIdx) = globalMaxRx;
  
  % estimate epsilon
  if nA(1) ~= 0 && nB(1) ~= 0 && nA(2) ~= 0 && nB(2) ~= 0
    epsEst = (nB(2) - nA(2))/(nB(1) - nA(1)) - 1;
    epsvec = [epsvec; epsEst];
    if mrx < 0.1*globalMaxRx
      if isempty(epsvec)
        epsEst = 0;
      else
        epsEst = epsvec(end-1);
        epsvec = epsvec(1:end-1);
      end
    end
    
    D = (nA(1)*nB(2) - nA(2)*nB(1))/(nB(2) - nA(2));
    nB = [0 0];
    timer = 0;
  end
  epsg(energyIdx) = epsEst;
  
  
  % timer such that rough cuts are at least 5s apart
  if timeoutFlag
    timer = timer + 1;
  end
  
  % upsample the asynchronous signal by 4 and interpolate with lagrange polynomials (Marcovich-Golan et al. 2017)
  upidx = 0;
  for nn = 0:4*hopSize-1
    if mod(nn,4) == 0
      upsbuffer = [upsbuffer(2:end); buffer(1+fftSize+upidx,2)];
      upidx = upidx + 1;
    else
      upsbuffer = [upsbuffer(2:end); 0];
    end
    upsframe(nn+1) = hup'*upsbuffer;
  end
  x2uphistory = [x2uphistory(4*hopSize+1:end); upsframe];
  for nn = 1:hopSize
    ndot = floor(4*(n + nn - 1)*(1 + epsEst));
    eta = 4*(n + nn - 1)*(1 + epsEst) - ndot;
    beta_1 = -eta*(eta - 1)*(eta - 2)/6;
    beta0 = (eta + 1)*(eta - 1)*(eta - 2)/2;
    beta1 = -(eta + 1)*eta*(eta - 2)/2;
    beta2 = (eta + 1)*eta*(eta - 1)/6;
    nndot = ndot - 4*n + 4*89*fftSize + 4*hopSize + 1;
    if eta ~= 0
      x2sync(n + nn - 1) = beta_1*x2uphistory(nndot-1) + beta0*x2uphistory(nndot) + beta1*x2uphistory(nndot+1) + beta2*x2uphistory(nndot+2);
    else
      x2sync(n + nn - 1) = buffer(nn+fftSize-floor(L/4),2);
    end
  end
  x1out(n:n+hopSize-1) = buffer(fftSize+1-floor(L/4):hopSize+fftSize-floor(L/4),1);
  
%  % calculate phase shift for synchronization
%  if epsEst ~= 0
%    n2sync = (1 + epsEst)*n;%(n - D);
%    nfloorfrac = n2sync - floor(n2sync);
%    nceilfrac = n2sync - ceil(n2sync);
%    [mm,j] = min([abs(nfloorfrac), abs(nceilfrac)]);
%    if j == 1
%      phibar = floor(n2sync);
%    elseif j == 2
%      phibar = ceil(n2sync);
%    end
%    ndelay = phibar - n;
%    phishift = n2sync - phibar;
%  end
  
%  % calculate delay for x1
%  if ndelay ~= 0
%    buffer2out = buffer(2*fftSize+ndelay+1:3*fftSize+ndelay,2);
%    buffer1out = buffer(2*fftSize+1:3*fftSize,2);
%  else
%    buffer2out = buffer(2*fftSize+1:3*fftSize,2);
%    buffer1out = buffer(2*fftSize+1:3*fftSize,1);
%  end
%  
%  % OLA FFT and sync
%  x2fft = fft(w.*buffer2out);
%  k = 0:fftSize/2;
%  phi = exp(1i*2*pi*k'*phishift/fftSize);
%  x2fftsync = x2fft(1:fftSize/2+1).*phi;
%  x2fftsync = [x2fftsync; flip(conj(x2fftsync(2:end-1)))];
%  outIdx = n:n+fftSize-1;
%  x2rec = real(ifft(x2fftsync));
%  x2sync(outIdx) = x2sync(outIdx) + scale*x2rec;
%  x1out(outIdx(1:hopSize)) = x1out(outIdx(1:hopSize)) + buffer1out(1:hopSize);
  
  energyIdx = energyIdx + 1;
endfor
x2sync = x2sync(1:N);
x1out = x1out(1:N);

%figure
%plot(10*log10(maxrx))
%hold on
%plot(10*log10(gmrx))
%plot(1e6*epsg)


%tS = (0:length(energy(:,1))-1)/(fs/hopSize);
%tS2 = (0:length(energy(:,1))-1)/(fs2/hopSize);
%figure
%plot(tS,10*log10(energy(:,1)),tS2,10*log10(energy(:,2)),tS,10*log10(noise(:,1)),tS2,10*log10(noise(:,2)),tS,10*vad(:,1),tS2,10*vad(:,2))
%grid
figure
plot(x1out(1:48e4))
hold on
plot(x2sync(1:48e4))