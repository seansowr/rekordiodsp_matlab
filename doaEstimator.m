%% DOA estimation based on Tho et al 2014

classdef doaEstimator
  properties
    NumMics;
    Rxx;
    avgCoef;
    c;
    fs;
    eps;
  end
  
  methods
    
    function obj = doaEstimator(nMics, soundSpeed, sampleRate)
      obj.NumMics = nMics;
      obj.Rxx = zeros(nMics);
      obj.avgCoef = 0.99;
      obj.c = soundSpeed;
      obj.fs = sampleRate;
      obj.eps = 1e-5;
    end
    
    function theta = estimateAngle(obj, x, r)
      obj.Rxx = obj.avgCoef*obj.Rxx + (1 - obj.avgCoef)*x*x'; % Autocovariance matrix
      [U, v] = eig(obj.Rxx);                                  % Get eigenvectors
      
      % get largest eigenvector
      v = diag(v);
      [~,idx] = max(abs(v));
      u = U(:,idx);
      
      % Estimate steering vector
      unor = u*conj(u(1))/(u(1)*conj(u(1)));
      
      if unor(1)~=0 && unor(2)~=0
        % Estimate phi from exp(i*phi)
        phi1 = angle(unor(2));
        phi2 = angle(unor(3));
        theta = atan(phi2/(phi1+obj.eps));
      else
        % if elements of u are zero just write in some nonsense
        theta = 2*pi + 1;
      end
    end
  end
end
