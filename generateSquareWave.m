%% Generate an un-aliased square wave
function s = generateSquareWave(f, fs, t)
  % function generateSquareWave
  % inputs:
  % f - frequency of the fundamental in Hz
  % fs - sampling frequency in Hz
  % t - duration of the generated signal in seconds
  % outputs:
  % s - the un-aliased square wave
  k = 1;
  N = floor(t*fs);
  n = 0:(N-1);
  s = zeros(N,1);
  while (2*k-1)*f < fs/2
    h =(4/pi)*sin(2*pi*(2*k-1)*(f/fs)*n')/(2*k-1);
    s = s + h;
    k = k + 1;
  endwhile
endfunction
