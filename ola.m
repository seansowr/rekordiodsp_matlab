fftSize = 512;
fs = 16e3;
f = 100;
N = fftSize*4;
n = (0:N-1)';
x = sin(2*pi*f*n/fs);
hopSize = fftSize/2;
w = hamming(fftSize);
w(1) = w(1)/2;
w(end) = w(end)/2;
out = zeros(length(x),1);
for k=1:hopSize:N-hopSize
  idx = k:k+fftSize-1;
  fftIn = w.*x(idx);
  xfft = fft(fftIn);
  xrec = ifft(xfft);
  out(idx) = out(idx) + xrec;
endfor
