%% new doa test
function results = newDoaTest(x, r, fs);
  % function newDoaTest()
  % inputs:
  % x - matrix of observed signals at each microphone
  % r - vector of mic distances from the origin
  % fs - sampling rate
  % outputs:
  % results - vector composed of DOA statistics [median mean variance]
  
  NFFT = 512;                         % FFT size
  NumBins = NFFT/2 + 1;               % Number of complex bins
  NumFrames = floor(length(x)/NFFT);  % Number of frames in simulation
  estimators = cell(NumBins,1);       % cell array to hold estimators
  NumMics = min(size(x));             % Number of microphones
  c = 343;                            % Speed of sound (m/s)
  
  % initialize estimators (1 per bin)
  for k = 1:NumBins
    estimators{k} = doaEstimator(NumMics, c, fs);
  end
  
  % simple SNR estimator parameters
  noiseFloor = 10^(-80/10)*ones(NumBins,NumMics);
  speechPwr = noiseFloor;
  noiseDecay = 0.9999;
  speechDecay = 0.995;
  
  thetas = [];  % array to hold DOA estimations
  
  % loop over the duration of simulation
  for l = 0:NumFrames-2
    idx = l*NFFT + 1;
    xfft = fft(x(idx:idx+512,:));
    
    % loop over appropriate frequency bins (depends on mic distances)
    for k = 4:12
      x_k = xfft(k,:);
      
      % estimate SNR
      noiseFloor(k,:) = noiseDecay*noiseFloor(k,:) + (1 - noiseDecay)*x_k;
      speechPwr(k,:) = speechDecay*speechPwr(k,:) + (1 - speechDecay)*x_k;
      if mean(speechPwr(k,:)) > mean(noiseFloor(k,:))
        % if SNR > 0dB estimate DOA
        theta = estimators{k}.estimateAngle(x_k', r);
        for m = 1:length(theta)
          if abs(theta) <= 2*pi
            thetas = [thetas; theta];
          end
        end
      end
      
    end
  end
  
  % Display histogram
  figure
  hist(thetas*180/pi,10,1);
  
  % collect results
  med = median(thetas);
  mu = mean(thetas);
  sgmsq = var(thetas);
  
  results = [med, mu, sgmsq];    
  
end